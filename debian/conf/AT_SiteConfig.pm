# Any configuration directives you include  here will override 
# AT's default configuration file, AT_Config.pm
#
# To include a directive here, just copy the equivalent statement
# from AT_Config.pm and change the value. We've included a single
# sample value below.
#
# This file is actually a perl module, so you can include valid
# perl code, as well.
#
# The converse is also true, if this file isn't valid perl, you're
# going to run into trouble. To check your SiteConfig file, use
# this comamnd:
#
#   perl -c /path/to/your/etc/AT_SiteConfig.pm
#
# You must restart your webserver after making changes to this file.


# If you don't want to use IPs, or would rather use CFs for IPs
# then set this to zero to disable IP features in AT
#Set ($EnableIP, 0);

1;
