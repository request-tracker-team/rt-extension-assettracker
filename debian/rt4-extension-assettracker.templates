# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: rt4-extension-assettracker/modify-database-permission
Type: select
__Choices: allow, prompt, deny
Default: allow
_Description: Permission to modify the Request Tracker database:
 Asset Tracker needs some modifications in the Request Tracker
 database to be functional. These modifications can be made
 automatically (choose "allow") or you may be prompted when they are
 needed (choose "prompt").
 Alternatively, you can run the necessary commands manually (choose "deny").
 .
 Please check the README.Debian file for more details.

Template: rt4-extension-assettracker/setup-database-prompt
Type: boolean
Default: true
_Description: Set up the Request Tracker database?
 New tables must be created in the Request Tracker database for Asset
 Tracker to be functional.

Template: rt4-extension-assettracker/upgrade-database-prompt
Type: boolean
Default: true
_Description: Upgrade the Request Tracker database?
 The Request Tracker database schema and contents must be upgraded for
 this version of Asset Tracker.

Template: rt4-extension-assettracker/modify-database-error
Type: select
__Choices: abort, retry, ignore
Default: abort
#flag:translate!:3
_Description: Action after database modification error:
 An error occurred while modifying the database:
 .
 ${error}
 .
 The full output should be available in Request Tracker log, most probably
 syslog.
 .
 You can retry the modification, abort the installation or ignore the
 error. If you abort the installation, the operation will fail and you
 will need to manually intervene (for instance by purging and
 reinstalling). If you choose to ignore the error, the upgrade process
 will continue.
